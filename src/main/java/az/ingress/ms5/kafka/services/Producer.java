package az.ingress.ms5.kafka.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Slf4j
@Service
@RequiredArgsConstructor
public class Producer {

    private final KafkaTemplate<String, String> kafkaTemplate;

    private long messageId;

    @Scheduled(fixedRate = 1000)
    public void produceMessages() {
        log.info("Sending new message to kafka");
        messageId++;
        kafkaTemplate.send("ms5-demo", messageId + "Hello Kafka, " + LocalDateTime.now());
    }

}
